<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Encuesta</title>

        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:100,600">

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/jqvmap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/flag-icon-css/css/flag-icon.min.css')}}">
        <!-- Materialize-->
        <link rel="stylesheet" type="text/css" href="{{asset('css/admin-materialize.min.css')}}">
        <!-- Material Icons-->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    </head>

    <style>
        .container-login{
            position: absolute;
            height: 100%;
            width: 50%;
            z-index: 1;
            right: 0;
            left: auto;
            background-color: #f5f5f5 !important;
            opacity: 1;
            padding-top: 7%;
            padding-bottom: 7%;
        }
        .background{
            display: block;
            background-image: url("{{asset('img/trees.jpg')}}");
            position: absolute;
            height: 100%;
            width: 100%;
            z-index: -1;
        }
        .container-phone{
            position: absolute;
            top: 50%;
            left: 25%;
            transform: translate(-50%, -50%);
            max-width: 300px;
            width: 100%;
            max-height: 600px;
            height: 100%;
            margin: 0 auto;
        }
        .phone{
            background-image: url({{asset('img/nexus5x.png')}});
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
            background-repeat: no-repeat;
            background-size: 100%;
            z-index: 2;
        }
        .container-img{
            position: absolute;
            background-size: cover;
            background-position: center;
            opacity: 1;
            z-index: 1;
            transition: opacity .3s;
            width: 92%;
            top: 9%;
            left: 4%;
            bottom: 10%;
        }
    </style>

    <body>
        <header>

        </header>
        <main>
            <div class="background"></div>
            <div class="container-phone">
                <div class="phone"></div>
                <div class="container-img" style="background-image: url({{asset('img/poly6.jpg')}})"></div>
            </div>
            <div class="container-login">
                <div class="row center-align">
                    <h1 class="purple-text text-darken-4" style="font-weight: 400; font-size: 6rem; text-transform: uppercase; margin: 1rem 0;">encuesta!</h1>

                    <div class="col s8 offset-s2">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title">Iniciar Sesión</span>
                                <div class="row">
                                    <form class="col s12">
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="email" type="email" class="validate">
                                                <label for="email">Email</label>
                                                <span class="helper-text" data-error="correo inválido" data-success="correcto"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <input id="password" type="password" class="validate">
                                                <label for="password">Password</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <a class="waves-effect waves-light btn red darken-4" href="{{route('home')}}" style="width: 100%">ingresar</a>
                                        </div>
                                        <div class="row">
                                            <a class="waves-effect waves-light btn purple darken-4" href="{{route('home')}}" style="width: 100%">registrar</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer>

        </footer>

        <!-- Scripts -->
        <script rel="script" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script rel="script" type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
        <script rel="script" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>

        <!-- Initialization script -->
        <script rel="script" type="text/javascript" src="{{asset('js/admin.js')}}"></script>
        <script rel="script" type="text/javascript" src="{{asset('js/init.js')}}"></script>
    </body>
</html>
