@extends('template.app')

@section('container')
    <div class="grey darken-4">
        <div class="container">
            <div class="row">
                <div class="col s9 m3 l3 xl2 offset-s3" style="padding-top: 15px">
                    <a class="btn-floating btn-large waves-effect waves-light red darken-4 modal-trigger" href="#modal1">
                        <i class="material-icons">add</i>

                    </a>
                    <form method="POST" class="form-horizontal" role="form" action="{{route('users.store')}}">
                    {{ csrf_field() }}
                    @include('users.create')

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="grey lighten-4">
        <div class="container">
            <div class="row">
                <p style="font-weight: bold">Usuarios</p>
            </div>
            <div class="row">
                @foreach($users as $user)
                <div class="col s12 m4 l4 xl3">
                    <div class="card">
                        <div class="card-content red darken-4">
                            <h2 class="white-text bold center-align">{{$user->nombre[0].$user->apellidos[0]}}</h2>
                        </div>
                        <div class="card-action">
                            <span style="font-weight: 500">{{$user->nombre.' '.$user->apellidos}}
                                <a class="dropdown-trigger" href="#" data-target="menu"><i class="material-icons right black-text">more_vert</i></a>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Dropdown Structure -->
                <ul id="menu" class="dropdown-content">
                    <li><a class="grey-text text-darken-2" href="#!"><i class="material-icons small">text_format</i>Cambiar nombre</a></li>
                    <li><a class="grey-text text-darken-2" href="#!"><i class="material-icons small">folder_open</i>Abrir</a></li>
                    <li class="divider" tabindex="-1"></li>
                    <li>
                        <a class="grey-text text-darken-2"  data-method="delete" href="{{route('users.destroy',$user->id)}}"><i class="material-icons small">delete</i>Eliminar</a>
                    </li>
                </ul>
                @endforeach
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.dropdown-trigger');
            var instances = M.Dropdown.init(elems, {
                alignment : 'left'
            });
        });
    </script>
@endsection
