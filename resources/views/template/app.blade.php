<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Formularios de Encuestas</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:100,600">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jqvmap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/flag-icon-css/css/flag-icon.min.css')}}">
    <!-- Materialize-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin-materialize.min.css')}}">
    <!-- Material Icons-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">

</head>
<body>

<header>
    <ul id="sidenav-left" class="sidenav white">
        <li><a href="dashboard.html" class="logo-container">Admin<i class="material-icons left">spa</i></a></li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold waves-effect"><a class="collapsible-header">Usuarios<i class="material-icons chevron">chevron_left</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('users.create')}}" class="waves-effect">Registrar Usuario<i class="material-icons">web</i></a></li>
                            <li><a href="{{route('users.index')}}" class="waves-effect">Ver Usuarios<i class="material-icons">multiline_chart</i></a></li>
                        </ul>
                    </div>
                </li>
                <li class="bold waves-effect"><a class="collapsible-header">Formularios<i class="material-icons chevron">chevron_left</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="dashboard.html" class="waves-effect">Mis Formularios<i class="material-icons">web</i></a></li>
                            <li><a href="pages-fixed-chart.html" class="waves-effect">Estadisticas<i class="material-icons">multiline_chart</i></a></li>
                        </ul>
                    </div>
                </li>

                <li class="bold waves-effect active"><a class="collapsible-header">Account<i class="material-icons chevron">chevron_left</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="log-in.html" class="waves-effect">Log In<i class="material-icons">person</i></a></li>
                            <li><a href="settings.html" class="waves-effect">Settings<i class="material-icons">settings</i></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
    </ul>

    <div id="dropdown1" class="dropdown-content notifications">
        <!--
        <div class="notifications-title">notifications</div>
        <div class="card">
            <div class="card-content"><span class="card-title">Joe Smith made a purchase</span>
                <p>Content</p>
            </div>
            <div class="card-action"><a href="#!">view</a><a href="#!">dismiss</a></div>
        </div>
        <div class="card">
            <div class="card-content"><span class="card-title">Daily Traffic Update</span>
                <p>Content</p>
            </div>
            <div class="card-action"><a href="#!">view</a><a href="#!">dismiss</a></div>
        </div>
        <div class="card">
            <div class="card-content"><span class="card-title">New User Joined</span>
                <p>Content</p>
            </div>
            <div class="card-action"><a href="#!">view</a><a href="#!">dismiss</a></div>
        </div>
        -->
    </div>

    <div id="chat-dropdown" class="dropdown-content dropdown-tabbed">
        <!--
        <ul class="tabs tabs-fixed-width">
            <li class="tab col s3"><a href="#settings">Settings</a></li>
            <li class="tab col s3"><a href="#friends" class="active">Friends</a></li>
        </ul>
        -->
        <!--
        <div id="settings" class="col s12">
            <div class="settings-group">
                <div class="setting">Night Mode
                    <div class="switch right">
                        <label>
                            <input type="checkbox"><span class="lever"></span>
                        </label>
                    </div>
                </div>
                <div class="setting">Beta Testing
                    <label class="right">
                        <input type="checkbox"><span></span>
                    </label>
                </div>
            </div>
        </div>
        -->
        <!--
        <div id="friends" class="col s12">
            <ul class="collection flush">
                <li class="collection-item avatar">
                    <div class="badged-circle online"><img src="img/portrait1.jpg" alt="avatar" class="circle"></div><span class="title">Jane Doe</span>
                    <p class="truncate">Lo-fi you probably haven't heard of them</p>
                </li>
                <li class="collection-item avatar">
                    <div class="badged-circle"><img src="img/portrait2.jpg" alt="avatar" class="circle"></div><span class="title">John Chang</span>
                    <p class="truncate">etsy leggings raclette kickstarter four dollar toast</p>
                </li>
                <li class="collection-item avatar">
                    <div class="badged-circle"><img src="img/portrait3.jpg" alt="avatar" class="circle"></div><span class="title">Lisa Simpson</span>
                    <p class="truncate">Raw denim fingerstache food truck chia health goth synth</p>
                </li>
            </ul>
        </div>
        -->
    </div>

</header>
<nav class="navbar nav-extended no-padding">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">Bienvenido</a>
        <a href="#" data-target="sidenav-left" class="sidenav-trigger"><i class="material-icons black-text">menu</i></a>
        <ul id="nav-mobile" class="right">
            <li class="hide-on-med-and-down"><a href="#!" data-target="dropdown1" class="dropdown-trigger waves-effect"><i class="material-icons">notifications</i></a></li>
            <li><a href="#!" data-target="chat-dropdown" class="dropdown-trigger waves-effect"><i class="material-icons">settings</i></a></li>
        </ul>

    </div>
</nav>
@yield('container')


<!-- Scripts -->
<script rel="script" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script rel="script" type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
<script rel="script" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>

<!-- External libraries -->

<!-- jqvmap -->
<script rel="script" type="text/javascript" src="{{asset('js/jqvmap/jquery.vmap.min.js')}}"></script>
<script rel="script" type="text/javascript" src="{{asset('js/jqvmap/jquery.vmap.world.js')}}" charset="utf-8"></script>
<script rel="script" type="text/javascript" src="{{asset('js/jqvmap/jquery.vmap.sampledata.js')}}"></script>

<!-- ChartJS -->
<script rel="script" type="text/javascript" src="{{asset('js/Chart.js')}}"></script>
<script rel="script" type="text/javascript" src="{{asset('js/Chart.Financial.js')}}"></script>


<script rel="script" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.7.0/fullcalendar.min.js"></script>
<script rel="script" type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
<script rel="script" type="text/javascript" src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script rel="script" type="text/javascript" src="{{asset('js/masonry.pkgd.min.js')}}"></script>


<!-- Initialization script -->
<script rel="script" type="text/javascript" src="{{asset('js/admin.js')}}"></script>
<script rel="script" type="text/javascript" src="{{asset('js/init.js')}}"></script>
</body>
</html>
