@extends('template.app')

@section('container')
    <div class="grey darken-4">
        <div class="container">
            <div class="row">
                <div class="col s6 m3 l3 xl2 offset-s3">
                    <p class="flow-text white-text">Crear Formulario</p>
                    <a href="{{route('forms.create')}}">
                        <div class="card">
                            <div class="card-content">
                                <div class="container">
                                    <img class="responsive-img" src="{{asset('img/svg/add-plus-button.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="grey lighten-4">
        <div class="container">
            <div class="row">
                <p style="font-weight: bold">Mis Formularios</p>
            </div>
            <div class="row">
                @for($i = 1; $i <= 8; $i++)
                <div class="col s12 m4 l4 xl3">
                    <div class="card">
                        <div class="card-content red darken-4">
                            <h2 class="white-text bold center-align">E{{$i}}</h2>
                        </div>
                        <div class="card-action">
                            <span style="font-weight: 500">Nombre de la encuesta
                                <a class="dropdown-trigger" href="#" data-target="menu{{$i}}"><i class="material-icons right black-text">more_vert</i></a>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Dropdown Structure -->
                <ul id="menu{{$i}}" class="dropdown-content">
                    <li><a class="grey-text text-darken-2" href="#!"><i class="material-icons small">text_format</i>Cambiar nombre</a></li>
                    <li><a class="grey-text text-darken-2" href="#!"><i class="material-icons small">folder_open</i>Abrir</a></li>
                    <li class="divider" tabindex="-1"></li>
                    <li><a class="grey-text text-darken-2" href="#!"><i class="material-icons small">delete</i>Eliminar</a></li>
                </ul>
                @endfor
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.dropdown-trigger');
            var instances = M.Dropdown.init(elems, {
                alignment : 'left'
            });
        });
    </script>
@endsection
