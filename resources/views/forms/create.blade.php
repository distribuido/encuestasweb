<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Crear Encuesta</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:100,600">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jqvmap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/flag-icon-css/css/flag-icon.min.css')}}">
    <!-- Materialize-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin-materialize.min.css')}}">
    <!-- Material Icons-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<style>
    .switch label input[type=checkbox]:checked + .lever {
        background-color: #e1bee7 !important;
    }

    .switch label input[type=checkbox]:checked + .lever:before, .switch label input[type=checkbox]:checked + .lever:after {
        left: 18px;
    }

    .switch label input[type=checkbox]:checked + .lever:after {
        background-color: #4a148c !important;
    }
    .switch label .lever:before {
        background-color: rgba(38, 14, 166, 0.15) !important;
    }

    .switch label .lever:after {
        background-color: #F1F1F1 !important;
        -webkit-box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
        box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
    }

    /* label color */
    .input-field label {
        color: #000;
    }
    /* label focus color */
    .input-field input[type=text]:focus + label {
        color: #4a148c !important;
    }
    /* label underline focus color */
    .input-field input[type=text]:focus {
        border-bottom: 1px solid #4a148c !important;
        box-shadow: 0 1px 0 0 #4a148c !important;
    }
    /* valid color */
    .input-field input[type=text].valid {
        border-bottom: 1px solid #000;
        box-shadow: 0 1px 0 0 #000;
    }
    /* invalid color */
    .input-field input[type=text].invalid {
        border-bottom: 1px solid #000;
        box-shadow: 0 1px 0 0 #000;
    }
    /* icon prefix focus color */
    .input-field .prefix.active {
        color: #4a148c !important;
    }

    textarea.materialize-textarea:focus:not([readonly]) {
        border-bottom: 1px solid #4a148c !important;
        -webkit-box-shadow: 0 1px 0 0 #4a148c !important;
        box-shadow: 0 1px 0 0 #4a148c !important;
    }
</style>
<body class="purple lighten-5">
    <header></header>
    <nav class="nav-extended purple darken-3">
        <div class="nav-wrapper">
            <a href="#!" class="brand-logo"><i class="material-icons small">arrow_back</i> Nuevo Fomulario</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#"><i class="material-icons right">palette</i></a></li>
                <li><a href="#"><i class="material-icons right">settings</i></a></li>
                <li><a class="waves-effect waves-light btn white purple-text text-darken-3">Guardar</a></li>
                <li><a class="waves-effect waves-light btn white purple-text text-darken-3">Guardar y Enviar</a></li>
                <li><a href="#"><i class="material-icons right">more_vert</i></a></li>
            </ul>
        </div>
        <div class="nav-content">
            <div class="row"></div>
            <div class="row"></div>
            <span class="nav-title">Formulario sin título</span>
            <a class="btn-floating btn-large halfway-fab waves-effect waves-light red darken-4 modal-trigger" href="#modal1">
                <i class="material-icons">add</i>
            </a>
        </div>
    </nav>
    <form method="POST" class="form-horizontal" role="form" action="{{route('forms.store')}}">
        {{ csrf_field() }}
        <div style="position: relative; bottom: 3rem">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 l8 offset-m1 offset-l2">
                        <div class="card-panel">
                            <div class="row">

                                    <div class="row">
                                        <div class="input-field">
                                            <input type="hidden" name="formId" value="{{$form->id}}">
                                        </div>
                                        <div class="input-field col s12">
                                            <input placeholder="Titulo del formulario" id="first_name" type="text" class="validate" name="titulo_formulario" value="{{$form->titulo . $form->id}}" style="font-size: xx-large">
                                        </div>
                                        <div class="input-field col s12">
                                            <textarea class="materialize-textarea" name="descripcion_formulario" placeholder="Descripción del formulario">{{$form->descripcion}}</textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <ul class="collection with-header">
                                            @foreach($preguntas as $pregunta)
                                            <li class="collection-item">
                                                <div>{{$pregunta->titulo}}<a href="#!" class="secondary-content"><i class="material-icons red-text text-darken-4">settings_applications</i></a></div>
                                            </li>
                                            @endforeach

                                        </ul>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('forms.question')
    </form>

    <!-- Scripts -->
    <script rel="script" type="text/javascript" src="{{asset('js/encuesta.js')}}"></script>
    <script rel="script" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
    <script rel="script" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>

    <!-- External libraries -->

    <!-- jqvmap -->
    <script rel="script" type="text/javascript" src="{{asset('js/jqvmap/jquery.vmap.min.js')}}"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/jqvmap/jquery.vmap.world.js')}}" charset="utf-8"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/jqvmap/jquery.vmap.sampledata.js')}}"></script>

    <!-- ChartJS -->
    <script rel="script" type="text/javascript" src="{{asset('js/Chart.js')}}"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/Chart.Financial.js')}}"></script>


    <script rel="script" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.7.0/fullcalendar.min.js"></script>
    <script rel="script" type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/masonry.pkgd.min.js')}}"></script>


    <!-- Initialization script -->
    <script rel="script" type="text/javascript" src="{{asset('js/admin.js')}}"></script>
    <script rel="script" type="text/javascript" src="{{asset('js/init.js')}}"></script>

</body>
</html>
