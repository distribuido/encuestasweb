<div id="modal1" class="modal modal-fixed-footer">
    <form action="">
        <div class="modal-content">
            <div class="row">
                <div class="col s12">
                    <div class="row card-panel" style='padding-top: 0.3rem !important; padding-bottom: 0.3rem !important; margin-left: 0.2%; margin-right: 0.2%'>
                        <div class="input-field col s6">
                            <textarea id="textarea1" class="materialize-textarea" name="titulo_pregunta" placeholder="Pregunta" style="font-size: x-large"></textarea>
                        </div>
                        <div class="input-field col s6" style="margin-top: 1.65rem">
                            <select name="seleccion" style="font-size: x-large" onchange="addFieldButton(this.value)">
                                <option value="" disabled selected>Escoja una opción</option>
                                <optgroup label="Respuesta Abierta">
                                    <option value="1" >Respuesta de texto</option>
                                    <option value="2">Respuesta de número</option>
                                    <option value="3" >Respuesta de fecha</option>
                                </optgroup>
                                <optgroup label="Respuesta Cerrada">
                                    <option value="4">Selección única</option>
                                    <option value="5">Selección multiple</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div id="answers" class="col s12">
                            <ul id="questions_list">

                            </ul>
                            <div id="other">

                            </div>
                            <div id="field_buttons">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer switch">
            <label class="left-align">
                Obligatorio
                <input type="checkbox" name="required">
                <span class="lever"></span>
            </label>
            <button type="submit" class="modal-close waves-effect waves-red btn red darken-4">Agregar</button>
        </div>
    </form>

    <script>

    </script>
</div>
