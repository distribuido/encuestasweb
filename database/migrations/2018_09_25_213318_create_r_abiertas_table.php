<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRAbiertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_abiertas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');

            $table->integer('respuesta_id')->unsigned();

            $table->foreign('respuesta_id')->references('id')->on('respuestas')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_abiertas');
    }
}
