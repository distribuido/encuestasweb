<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreRequisitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_requisitos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->dateTime('created_at');

            $table->integer('opcion_id')->unsigned();
            $table->integer('m_pregunta_requisito_id')->unsigned();
            $table->integer('m_pregunta_requiere_id')->unsigned();

            $table->foreign('opcion_id')->references('id')->on('opciones')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('m_pregunta_requisito_id')->references('id')->on('m_preguntas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('m_pregunta_requiere_id')->references('id')->on('m_preguntas')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_requisitos');
    }
}
