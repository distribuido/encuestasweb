<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_preguntas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->integer('obligatorio');
            $table->timestamps();

            $table->integer('modelo_encuesta_id')->unsigned();

            $table->foreign('modelo_encuesta_id')->references('id')->on('modelo_encuestas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_preguntas');
    }
}
