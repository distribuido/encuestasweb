<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_respuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo');
            $table->char('nombre');
            $table->timestamps();

            $table->integer('m_pregunta_id')->unsigned();

            $table->foreign('m_pregunta_id')->references('id')->on('m_preguntas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_respuestas');
    }
}
