<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('valor');
            $table->integer('tipo_respuesta');
            $table->dateTime('created_at');

            $table->integer('encuesta_id')->unsigned();
            $table->integer('m_pregunta_id')->unsigned();
            $table->integer('dominio_id')->unsigned()->nullable();
            $table->integer('opcion_id')->unsigned();

            $table->foreign('encuesta_id')->references('id')->on('encuestas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('m_pregunta_id')->references('id')->on('m_preguntas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('dominio_id')->references('id')->on('dominios')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('opcion_id')->references('id')->on('opciones')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas');
    }
}
