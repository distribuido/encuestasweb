<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCerradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cerradas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo');
            $table->timestamps();

            $table->integer('m_respuesta_id')->unsigned();

            $table->foreign('m_respuesta_id')->references('id')->on('m_respuestas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cerradas');
    }
}
