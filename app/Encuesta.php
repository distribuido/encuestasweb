<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{
    protected $table = 'encuestas';

    public $timestamps = false;

    protected $fillable = [
        'descripcion', 'created_at', 'modelo_encuesta_id'
    ];

    protected $hidden = [
        'id'
    ];

    public function respuestas()
    {
        return $this->hasMany('App\Respuesta');
    }

    public function modeloencuesta()
    {
        return $this->belongsTo('App\ModeloEncuesta');
    }

    public function m_modulos()
    {
        return $this->hasMany('App\MModulos');
    }
}
