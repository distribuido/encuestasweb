<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RAbierta extends Model
{
    protected $table = 'r_abiertas';

    public $timestamps = false;

    protected $fillable = [
        'respuesta', 'respuesta_id'
    ];

    protected $hidden = [
        'id'
    ];

    public function respuesta()
    {
        return $this->belongsTo('App\Respuesta');
    }
}
