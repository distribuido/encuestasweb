<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class Usuario extends Model
{
    use Authenticatable;

    protected $table = 'usuarios';

    protected $fillable = [
        'nombre', 'apellidos', 'ci'
    ];

    protected $hidden = [
        'password', 'id', 'remember_token'
    ];

    public $timestamps = false;

    public function modelo_encuestas()
    {
        return $this->hasMany('App\ModeloEncuesta');
    }
}
