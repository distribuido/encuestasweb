<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MRespuesta extends Model
{
    protected $table = 'm_respuestas';

    public $timestamps = true;

    protected $fillable = [
        'tipo', 'm_pregunta_id'
    ];

    protected $hidden = [

    ];

    public function m_pregunta()
    {
        return $this->belongsTo('App\MPregunta');
    }

    public function abiertas()
    {
        return $this->hasMany('App\Abierta');
    }

    public function cerradas()
    {
        return $this->hasMany('App\Cerrada');
    }
}
