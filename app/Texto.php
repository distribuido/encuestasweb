<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Texto extends Model
{
    protected $table = 'textos';

    public $timestamps = true;

    protected $fillable = [
        'min_longitud', 'max_longitud', 'dominio_id'
    ];

    protected $hidden = [

    ];

    public function dominio()
    {
        return $this->belongsTo('App\Dominio');
    }
}
