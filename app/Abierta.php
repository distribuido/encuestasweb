<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abierta extends Model
{

    protected $table = 'abiertas';

    public $timestamps = true;

    protected $fillable = [
        'id'
    ];

    protected $hidden = [

    ];

    public function m_respuesta()
    {
        return $this->belongsTo('App\MRespuesta');
    }

    public function dominios()
    {
        return $this->hasMany('App\Dominio');
    }
}
