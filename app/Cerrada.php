<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cerrada extends Model
{
    protected $table = 'cerradas';

    public $timestamps = true;

    protected $fillable = [
        'tipo', 'm_respuesta_id'
    ];

    protected $hidden = [

    ];

    public function m_respuesta()
    {
        return $this->belongsTo('App\MRespuesta');
    }

    public function opciones()
    {
        return $this->hasMany('App\Opcion');
    }
}
