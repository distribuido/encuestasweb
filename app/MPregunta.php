<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MPregunta extends Model
{
    protected $table = 'm_preguntas';

    public $timestamps = true;

    protected $fillable = [
        'titulo', 'm_modulo_id'
    ];

    protected $hidden = [

    ];

    public function m_encuesta()
    {
        return $this->belongsTo('App\ModeloEncuesta');
    }

    public function m_respuestas()
    {
        return $this->hasMany('App\MRespuesta');
    }

    public function pre_requisitos_requeridos()
    {
        return $this->hasMany('App\PreRequisitos');
    }

    public function pre_requisitos_requiere()
    {
        return $this->hasMany('App\PreRequisitos');
    }

}
