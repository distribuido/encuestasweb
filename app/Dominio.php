<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dominio extends Model
{

    protected $table = 'dominios';

    public $timestamps = true;

    protected $fillable = [
        'valor_default', 'tipo', 'abierta_id'
    ];

    protected $hidden = [

    ];

    public function abierta()
    {
        return $this->belongsTo('App\Abierta');
    }

    public function respuestas()
    {
        return $this->hasMany('App\Respuesta');
    }

    public function fechas()
    {
        return $this->hasMany('App\Fecha');
    }

    public function textos()
    {
        return $this->hasMany('App\Texto');
    }

    public function numeros()
    {
        return $this->hasMany('App\Numero');
    }
}
