<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreRequisito extends Model
{
    protected $table = 'pre_requisitos';

    public $timestamps = false;

    protected $fillable = [
        'descripcion', 'titulo', 'tipo_valor', 'created_at', 'm_pregunta_requisito_id', 'm_pregunta_requiere_id'
    ];

    protected $hidden = [
        'id'
    ];

    public function m_pregunta_requisito()
    {
        return $this->belongsTo('App\MPregunta');
    }

    public function m_pregunta_requiere()
    {
        return $this->belongsTo('App\MPregunta');
    }
}
