<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LogController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt(['usuarios' => $request['ci'], 'password' => $request['password']])) {
            return Redirect::route('home');
        } else {
            flash('Usuario o Contraseña incorrectos', 'danger');
            return Redirect::to('/');
        }
    }

    public function index()
    {
        return Redirect::to('/');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }
}
