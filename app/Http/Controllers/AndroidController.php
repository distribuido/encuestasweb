<?php

namespace App\Http\Controllers;

use App\Abierta;
use App\Cerrada;
use App\Dominio;
use App\Fecha;
use App\ModeloEncuesta;
use App\MPregunta;
use App\MRespuesta;
use App\Numero;
use App\Opcion;
use App\Texto;
use Illuminate\Http\JsonResponse;

class AndroidController extends Controller
{

    public function index(){
        $forms = ModeloEncuesta::all();
        foreach ($forms as $form){
            $questions = MPregunta::where('modelo_encuesta_id', '=', $form->id)->get();

            foreach ($questions as $question){
                $answers = MRespuesta::where('m_pregunta_id', '=', $question->id)->get();
                $question['respuestas'] = $answers;

                foreach ($answers as $answer){
                    //cerrada
                    if($answer->tipo == 1){
                        $type_answer = Cerrada::where('m_respuesta_id', '=', $answer->id)->first();
                        $answer['tipo_cerrado'] = $type_answer;

                        $options = Opcion::where('cerrada_id', '=', $type_answer->id)->get();
                        $type_answer['opciones'] = $options;
                    }
                    // abierta
                    else if($answer->tipo == 0){
                        $type_answer = Abierta::where('m_respuesta_id', '=', $answer->id)->first();
                        $answer['tipo_abierto'] = $type_answer;

                        $domain = Dominio::where('abierta_id', '=', $type_answer->id)->first();
                        $type_answer['dominio'] = $domain;

                        //texto
                        if($domain->tipo == 1){
                            $texto = Texto::where('dominio_id', '=', $domain->id)->first();
                            $domain['rango'] = $texto;
                        }
                        //numero
                        else if($domain->tipo == 2){
                            $numero = Numero::where('dominio_id', '=', $domain->id)->first();
                            $domain['rango'] = $numero;
                        }
                        //fecha
                        else if($domain->tipo == 3){
                            $fecha = Fecha::where('dominio_id', '=', $domain->id)->first();
                            $domain['rango'] = $fecha;
                        }
                    }
                }
            }
            $form['preguntas'] = $questions;
        }
        $response['status'] = 200;
        $response['data'] = $forms;

        return new JsonResponse($response);
    }

    public function headers(){
        $forms = ModeloEncuesta::all();
        $response['status'] = 200;
        $response['data'] = $forms;
        return new JsonResponse($response);
    }

    public function form($id){
        $form = ModeloEncuesta::where('id', '=', $id)->first();
        $questions = MPregunta::where('modelo_encuesta_id', '=', $form->id)->get();

        foreach ($questions as $question){
            $answers = MRespuesta::where('m_pregunta_id', '=', $question->id)->get();
            $question['respuestas'] = $answers;

            foreach ($answers as $answer){
                //cerrada
                if($answer->tipo == 1){
                    $type_answer = Cerrada::where('m_respuesta_id', '=', $answer->id)->first();
                    $answer['tipo_cerrado'] = $type_answer;

                    $options = Opcion::where('cerrada_id', '=', $type_answer->id)->get();
                    $type_answer['opciones'] = $options;
                }
                // abierta
                else if($answer->tipo == 0){
                    $type_answer = Abierta::where('m_respuesta_id', '=', $answer->id)->first();
                    $answer['tipo_abierto'] = $type_answer;

                    $domain = Dominio::where('abierta_id', '=', $type_answer->id)->first();
                    $type_answer['dominio'] = $domain;

                    //texto
                    if($domain->tipo == 1){
                        $texto = Texto::where('dominio_id', '=', $domain->id)->first();
                        $domain['rango'] = $texto;
                    }
                    //numero
                    else if($domain->tipo == 2){
                        $numero = Numero::where('dominio_id', '=', $domain->id)->first();
                        $domain['rango'] = $numero;
                    }
                    //fecha
                    else if($domain->tipo == 3){
                        $fecha = Fecha::where('dominio_id', '=', $domain->id)->first();
                        $domain['rango'] = $fecha;
                    }
                }
            }
        }
        $form['preguntas'] = $questions;
        $response['status'] = 200;
        $response['data'] = $form;

        return new JsonResponse($response);
    }
}
