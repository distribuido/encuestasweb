<?php

namespace App\Http\Controllers;

use App\Abierta;
use App\Cerrada;
use App\Dominio;
use App\Fecha;
use App\ModeloEncuesta;
use App\MPregunta;
use App\MRespuesta;
use App\Numero;
use App\Opcion;
use App\Texto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Array_;

class FormController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        $form = new ModeloEncuesta();
        $form->id = 0;
        $preguntas = [];
        return view('forms.create', ['form' => $form, 'preguntas' => $preguntas]);
    }

    public function store(Request $request)
    {
        if($request['formId'] == 0){
            $modelo_encuesta = new ModeloEncuesta();
            $modelo_encuesta->fecha_inicio = "2018-02-03";
            $modelo_encuesta->fecha_fin = "2018-03-03";
            $modelo_encuesta->cantidad_encuestados = 0;
            $modelo_encuesta->version = 1;
            //$modelo_encuesta->created_at = Carbon::now();
            $modelo_encuesta->usuario_id = 1;
        } else {
            $modelo_encuesta = ModeloEncuesta::find($request['formId']);
        }
        $modelo_encuesta->titulo = $request->titulo_formulario;
        $modelo_encuesta->subtitulo = 'subtitulo';
        $modelo_encuesta->descripcion = $request->descripcion_formulario;
        $modelo_encuesta->objetivo = "objetivo";
        $modelo_encuesta->save();


        $m_pregunta = new MPregunta();
        $m_pregunta->titulo = $request->titulo_pregunta;
        if ($request->required == 'on') {
            $m_pregunta->obligatorio = 1;//obligatorio
        } else {
            $m_pregunta->obligatorio = 0;//no obligarotio
        }
        $m_pregunta->modelo_encuesta_id = $modelo_encuesta->id;
        //$m_pregunta->created_at = Carbon::now();
        $m_pregunta->save();

        //Preguntamos si la PREGUNTA es Cerrada.
        if($request->seleccion == 4 || $request->seleccion == 5){
            //Creamos la respuesta que contenga las opciones cerradas
            $m_respuesta = new MRespuesta();
            $m_respuesta->nombre = 'default';
            //$m_respuesta->created_at = Carbon::now();
            $m_respuesta->tipo = 1; // cerrada
            $m_respuesta->m_pregunta_id = $m_pregunta->id;
            $m_respuesta->save();

            //Se crea el tipo cerrado y si es de unica o multiple
            $cerrada = new Cerrada();
            if ($request->seleccion == 4) {
                $cerrada->tipo = 0; //opcion unica
            } else if($request->seleccion == 5){
                $cerrada->tipo = 1; //opcion multiple
            }
            $cerrada->m_respuesta_id = $m_respuesta->id;
            //$cerrada->created_at = Carbon::now();
            $cerrada->save();

            for($i = 0; $i < count($request->answers); $i++){
                // se guarda las opciones del tipo de pregunta que es
                $opcion = new Opcion();
                $opcion->titulo = $request->answers[$i];
                $opcion->valor = 'default'; //borrar despues
                $opcion->tipo_valor = 0; //saber si es numero, texto, fecha
                //$opcion->created_at = Carbon::now();
                $opcion->cerrada_id = $cerrada->id;
                $opcion->save();
            }

            //preguntamos si existe un tipo de pregunta abierta dentro de una cerrada unica o multiple
            if($request->type != null){
                //creamos la respuesta
                $m_respuesta = new MRespuesta();
                $m_respuesta->nombre = 'default';
                $m_respuesta->tipo = 0; // respuesta abierta
                //$m_respuesta->created_at = Carbon::now();
                $m_respuesta->m_pregunta_id = $m_pregunta->id;
                $m_respuesta->save();

                //creamos el tipo abierto
                $abierta = new Abierta();
                $abierta->m_respuesta_id = $m_respuesta->id;
                //$abierta->created_at = Carbon::now();
                $abierta->save();

                //creamos el dominio de la respuesta
                $dominio = new Dominio();
                $dominio->valor_default = 'default';//borrar luego
                $dominio->tipo = $request->type;
                //$dominio->created_at = Carbon::now();
                $dominio->abierta_id = $abierta->id;
                $dominio->save();

                //creamos el tipo de dominio
                if ($request->type == 1) {
                    //texto
                    $texto = new Texto();
                    $texto->max_longitud = $request->text_max;
                    //$texto->created_at = Carbon::now();
                    $texto->dominio_id = $dominio->id;
                    $texto->save();
                } else if ($request->type == 2) {
                    //numero
                    $numero = new Numero();
                    $numero->min_valor = $request->number_min;
                    $numero->max_valor = $request->number_max;
                    //$numero->created_at = Carbon::now();
                    $numero->dominio_id = $dominio->id;
                    $numero->save();
                } //fecha
                else {
                    $fecha = new Fecha();
                    $fecha->min_fecha = $request->date_min;
                    $fecha->max_fecha = $request->date_max;
                    //$fecha->created_at = Carbon::now();
                    $fecha->dominio_id = $dominio->id;
                    $fecha->save();
                }
            }

        } else if($request->seleccion == 1 || $request->seleccion == 2 || $request->seleccion == 3){
            $m_respuesta = new MRespuesta();
            $m_respuesta->nombre = 'default';
            $m_respuesta->tipo = 0; // respuesta abierta


            $m_respuesta->m_pregunta_id = $m_pregunta->id;
            //$m_respuesta->created_at = Carbon::now();
            $m_respuesta->save();

            $abierta = new Abierta();
            $abierta->m_respuesta_id = $m_respuesta->id;
            //$abierta->created_at = Carbon::now();
            $abierta->save();

            $dominio = new Dominio();
            $dominio->valor_default = 'default';//borrar luego
            $dominio->tipo = $request->seleccion;
            //$dominio->created_at = Carbon::now();
            $dominio->abierta_id = $abierta->id;
            $dominio->save();

            if ($request->seleccion == 1) {
                //texto
                $texto = new Texto();
                $texto->max_longitud = $request->longitud_max;
                $texto->dominio_id = $dominio->id;
                //$texto->created_at = Carbon::now();
                $texto->save();
            } else if ($request->seleccion == 2) {
                //numero
                $numero = new Numero();
                $numero->min_valor = $request->numero_min;
                $numero->max_valor = $request->numero_max;
                $numero->dominio_id = $dominio->id;
                //$numero->created_at = Carbon::now();
                $numero->save();
            } //fecha
            else if($request->seleccion == 3){
                $fecha = new Fecha();
                $fecha->min_fecha = $request->fecha_min;
                $fecha->max_fecha = $request->fecha_max;
                $fecha->dominio_id = $dominio->id;
                //$fecha->created_at = Carbon::now();
                $fecha->save();
            }
        }

        return redirect()->route('forms.show', $modelo_encuesta->id);

    }

    public function show($id){
        $encuesta = ModeloEncuesta::find($id);
        $preguntas = $encuesta->m_preguntas;
        return view('forms.create', ['form' => $encuesta, 'preguntas' => $preguntas]);
    }

    public function edit($id){

    }

    public function update(Request $request, $id){
        //
    }

    public function destroy($id){
        //
    }
}
