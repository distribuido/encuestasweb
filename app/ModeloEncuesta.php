<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModeloEncuesta extends Model
{

    protected $table = 'modelo_encuestas';

    public $timestamps = true;

    protected $fillable = [
        'titulo', 'subtitulo', 'descripcion', 'objetivo', 'fecha_fin', 'cantidad_encuestados', 'usuario_id'
    ];

    protected $hidden = [

    ];

    public function usuario()
    {
        return $this->belongsTo('App\Usuario');
    }

    public function m_preguntas()
    {
        return $this->hasMany('App\MPregunta');
    }

    public function encuestas()
    {
        return $this->hasMany('App\Encuesta');
    }


}
