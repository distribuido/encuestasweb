<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Numero extends Model
{
    protected $table = 'numeros';

    public $timestamps = true;

    protected $fillable = [
        'min_valor', 'max_valor', 'dominio_id'
    ];

    protected $hidden = [

    ];

    public function dominio()
    {
        return $this->belongsTo('App\Dominio');
    }
}
