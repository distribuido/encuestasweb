<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opcion extends Model
{

    protected $table = 'opciones';

    public $timestamps = true;

    protected $fillable = [
        'valor', 'titulo', 'tipo_valor', 'tipo', 'cerrada_id'
    ];

    protected $hidden = [

    ];

    public function cerrada()
    {
        return $this->belongsTo('App\Cerrada');
    }

    public function pre_requisitos()
    {
        return $this->hasMany('App\PreRequisito');
    }

    public function respuestas()
    {
        return $this->hasMany('App\Respuesta');
    }


}
