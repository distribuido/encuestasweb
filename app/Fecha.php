<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fecha extends Model
{
    protected $table = 'fechas';

    public $timestamps = true;

    protected $fillable = [
        'min_fecha', 'formato', 'max_fecha', 'cerrada_id', 'dominio_id'
    ];

    protected $hidden = [

    ];

    public function dominio()
    {
        return $this->belongsTo('App\Dominio');
    }
}
