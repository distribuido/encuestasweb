<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RCerrada extends Model
{
    protected $table = 'r_cerradas';

    public $timestamps = false;

    protected $fillable = [
        'titulo', 'respuesta_id'
    ];

    protected $hidden = [
        'id'
    ];

    public function respuesta()
    {
        return $this->belongsTo('App\Respuesta');
    }
}
