<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{

    protected $table = 'respuestas';

    public $timestamps = false;

    protected $fillable = [
        'tipo', 'created_at', 'encuesta_id', 'm_pregunta_id', 'dominio_id', 'opcion_id'
    ];

    protected $hidden = [
        'id'
    ];

    public function encuesta()
    {
        return $this->belongsTo('App\Encuesta');
    }

    public function m_pregunta()
    {
        return $this->belongsTo('App\MPregunta');
    }

    public function dominio()
    {
        return $this->belongsTo('App\Dominio');
    }

    public function opcion()
    {
        return $this->belongsTo('App\Opcion');
    }

    public function r_abiertas()
    {
        return $this->hasMany('App\RAbierta');
    }

    public function r_cerradas()
    {
        return $this->hasMany('App\RCerrada');
    }
}
