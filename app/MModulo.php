<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MModulo extends Model
{

    protected $table = 'm_modulos';

    public $timestamps = false;

    protected $fillable = [
        'titulo', 'descripcion', 'created_at', 'modelo_encuesta_id'
    ];

    protected $hidden = [
        'id'
    ];

    public function modelo_encuesta()
    {
        return $this->belongsTo('App\ModeloEncuesta');
    }

    public function m_preguntas()
    {
        return $this->hasMany('App\MPregunta');
    }
}
