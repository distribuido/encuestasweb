var indexSingle = 1;
var indexMultiple = 1;
function addQuestionOpen(type){
    var fieldAnswerOpen = '';
    if(type === '1'){
        fieldAnswerOpen = '<div class="row card-panel" style="margin-left: 0.2rem !important; margin-right: 0.2rem !important;">' +
            '                        <div class="col s6">' +
            '                            <div class="input-field">' +
            '                                <input disabled placeholder="Respuesta abierta..." type="text" class="validate">' +
            '                            </div>' +
            '                        </div>' +
            '                        <div class="col s6">' +
            '                            <div class="row">' +
            '                                <div class="input-field col s6 offset-s6">' +
            '                                    <input placeholder="maxima logitud" type="text" class="validate" name="longitud_max">' +
            '                                </div>' +
            '                            </div>' +
            '                        </div>' +
            '                    </div>';
    }else if(type === '2'){
        fieldAnswerOpen = '<div class="row card-panel" style="margin-left: 0.2rem !important; margin-right: 0.2rem !important;">' +
            '                        <div class="col s6">' +
            '                            <div class="input-field">' +
            '                                <input disabled placeholder="Respuesta abierta..." type="text" class="validate">' +
            '                            </div>' +
            '                        </div>' +
            '                        <div class="col s6">' +
            '                            <div class="row">' +
            '                                <div class="input-field col s6">' +
            '                                    <input placeholder="minimo" type="text" class="validate" name="numero_min">' +
            '                                </div>' +
            '                                <div class="input-field col s6">' +
            '                                    <input placeholder="maximo" type="text" class="validate" name="numero_max">' +
            '                                </div>' +
            '                            </div>' +
            '                        </div>' +
            '                    </div>';
    } else if(type === '3'){
        fieldAnswerOpen = '<div class="row card-panel" style="margin-left: 0.2rem !important; margin-right: 0.2rem !important;">' +
            '                        <div class="col s6">' +
            '                            <div class="input-field">' +
            '                                <input disabled placeholder="Respuesta abierta..." type="text" class="validate">' +
            '                            </div>' +
            '                        </div>' +
            '                        <div class="col s6">' +
            '                            <div class="row">' +
            '                                <div class="input-field col s6">' +
            '                                    <input placeholder="minimo" type="date" class="validate" name="fecha_min">' +
            '                                </div>' +
            '                                <div class="input-field col s6">' +
            '                                    <input placeholder="maximo" type="date" class="validate" name="fecha_max">' +
            '                                </div>' +
            '                            </div>' +
            '                        </div>' +
            '                    </div>';
    }
    document.getElementById('other').innerHTML = fieldAnswerOpen;

}
function addQuestionSingle() {
    indexSingle++;
    var newQuestion =
        "<div class='card-panel' style='padding-top: 0.3rem !important; padding-bottom: 0.3rem !important;'>" +
        "<div class='input-field'>" +
        "<i class='material-icons prefix'>adjust</i>" +
        "<input placeholder='Respuesta' id='answer-" + indexSingle + "' name='answers[]' type='text' class='validate' value='opción' style='width: 92%;'>" +
        "<a class='right' href='#' style='margin-top: 1.5rem' onclick='deleteQuestion(" + indexSingle + ")'><i class='material-icons red-text text-darken-4'>delete</i></a>" +
        "</div>" +
        "</div>";

    var newLi = document.createElement('li');
    newLi.id = 'question-' + indexSingle;
    newLi.innerHTML = newQuestion;
    document.getElementById('questions_list').appendChild(newLi);
}
function deleteQuestion(pos) {
    node = document.getElementById('question-'+pos);
    node.parentNode.removeChild(node);
}
function addQuestionMultiple() {
    indexMultiple++;
    var newQuestion =
        "<div class='card-panel' style='padding-top: 0.3rem !important; padding-bottom: 0.3rem !important;'>" +
        "<div class='input-field'>" +
        "<i class='material-icons prefix'>check_box_outline_blank</i>" +
        "<input placeholder='Respuesta' id='answer-" + indexMultiple + "' name='answers[]' type='text' class='validate' value='opción' style='width: 92%;'>" +
        "<a class='right' href='#' style='margin-top: 1.5rem' onclick='deleteQuestion(" + indexMultiple + ")'><i class='material-icons red-text text-darken-4'>delete</i></a>" +
        "</div>" +
        "</div>";

    var newLi = document.createElement('li');
    newLi.id = 'question-' + indexMultiple;
    newLi.innerHTML = newQuestion;
    document.getElementById('questions_list').appendChild(newLi);
}

//mejorar este evento para uno mas globar y refesh de vista
function addFieldButton(type){
    var fieldButton = '';
    if(type === '4'){
        console.log('click' + '33');
        fieldButton = '<a class="waves-effect waves-light btn white purple-text text-darken-3" onclick="addQuestionSingle()">Añadir opcion</a>' +
            '<a class="waves-effect waves-light btn white purple-text text-darken-3" onclick="addOtherSingle()">Añadir respuesta "otro"</a>';
    } else if(type === '5') {
        console.log('click' + '44');
        fieldButton = '<a class="waves-effect waves-light btn white purple-text text-darken-3" onclick="addQuestionMultiple()">Añadir opcion</a>' +
            '<a class="waves-effect waves-light btn white purple-text text-darken-3" onclick="addOtherMultiple()">Añadir respuesta "otro"</a>';
    } else if(type === '1' || type === '2' || type === '3'){
        addQuestionOpen(type);
    }
    document.getElementById('field_buttons').innerHTML = fieldButton;
}
function addOtherSingle() {
    indexSingle++;
    var fieldOther =
        '<div class="card-panel" style="padding-top: 0.3rem !important; padding-bottom: 0.3rem !important;">' +
        '<div class="row">' +
        '<div class="input-field col s12">' +
        '<i class="material-icons prefix">adjust</i>' +
        '<input disabled value="otra..." id="disabled" type="text" class="validate" name="other" style="width: 92%;">' +
        '<a class="right" href="#" style="margin-top: 1.5rem" onclick="deleteQuestion(' + indexSingle + ')"><i class="material-icons red-text text-darken-4">delete</i></a>' +
        '</div>' +
        '<div class="col s3">' +
        '<div class="row">' +
        '<div class="col s4">' +
        '<p>' +
        '<label>' +
        '<input class="with-gap" name="type" type="radio" checked  value="1"/>' +
        '<span class="black-text">Texto</span>' +
        '</label>' +
        '</p>' +
        '</div>' +
        '<div class="col s8">' +
        '<input placeholder="Máximo" type="number" class="validate" name="text_max">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col s4">' +
        '<div class="row">' +
        '<div class="col s4">' +
        '<p>' +
        '<label>' +
        '<input class="with-gap" name="type" type="radio"  value="2"/>' +
        '<span class="black-text">Número</span>' +
        '</label>' +
        '</p>' +
        '</div>' +
        '<div class="col s4">' +
        '<input placeholder="Mínimo" type="number" class="validate" name="number_min">' +
        '</div>' +
        '<div class="col s4">' +
        '<input placeholder="Máximo" type="number" class="validate" name="number_max">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col s5">' +
        '<div class="row">' +
        '<div class="col s3">' +
        '<p>' +
        '<label>' +
        '<input class="with-gap" name="type" type="radio" value="3"/>' +
        '<span class="black-text">Fecha</span>' +
        '</label>' +
        '</p>' +
        '</div>' +
        '<div class="col s5">' +
        '<input type="date" name="date_min">' +
        '</div>' +
        '<div class="col s4">' +
        '<input type="date" name="date_max">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    var newLi = document.createElement('li');
    newLi.id = 'question-' + indexSingle;
    newLi.innerHTML = fieldOther;
    document.getElementById('questions_list').appendChild(newLi);
}
function addOtherMultiple() {
    indexMultiple++;
    var fieldOther =
        '<div class="card-panel" style="padding-top: 0.3rem !important; padding-bottom: 0.3rem !important;">' +
        '<div class="row">' +
        '<div class="input-field col s12">' +
        '<i class="material-icons prefix">check_box_outline_blank</i>' +
        '<input disabled value="otra..." id="disabled" type="text" class="validate" name="other" style="width: 92%;">' +
        '<a class="right" href="#" style="margin-top: 1.5rem" onclick="deleteQuestion(' + indexMultiple + ')"><i class="material-icons red-text text-darken-4">delete</i></a>' +
        '</div>' +
        '<div class="col s3">' +
        '<div class="row">' +
        '<div class="col s4">' +
        '<p>' +
        '<label>' +
        '<input class="with-gap" name="type" type="radio" checked  value="1"/>' +
        '<span class="black-text">Texto</span>' +
        '</label>' +
        '</p>' +
        '</div>' +
        '<div class="col s8">' +
        '<input placeholder="Máximo" type="number" class="validate" name="text_max">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col s4">' +
        '<div class="row">' +
        '<div class="col s4">' +
        '<p>' +
        '<label>' +
        '<input class="with-gap" name="type" type="radio"  value="2"/>' +
        '<span class="black-text">Número</span>' +
        '</label>' +
        '</p>' +
        '</div>' +
        '<div class="col s4">' +
        '<input placeholder="Mínimo" type="number" class="validate" name="number_min">' +
        '</div>' +
        '<div class="col s4">' +
        '<input placeholder="Máximo" type="number" class="validate" name="number_max">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col s5">' +
        '<div class="row">' +
        '<div class="col s3">' +
        '<p>' +
        '<label>' +
        '<input class="with-gap" name="type" type="radio" value="3"/>' +
        '<span class="black-text">Fecha</span>' +
        '</label>' +
        '</p>' +
        '</div>' +
        '<div class="col s5">' +
        '<input type="date" name="date_min">' +
        '</div>' +
        '<div class="col s4">' +
        '<input type="date" name="date_max">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    var newLi = document.createElement('li');
    newLi.id = 'question-' + indexMultiple;
    newLi.innerHTML = fieldOther;
    document.getElementById('questions_list').appendChild(newLi);
}